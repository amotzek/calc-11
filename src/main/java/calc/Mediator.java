package calc;
/*
 * Copyright (C) 2015, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.LinkedList;
/*
 * Created by andreasm on 21.09.15.
 */
public final class Mediator
{
    private final HashMap<Class, Object> singletonbyclass;
    private final LinkedList<Object> objects;
    //
    public Mediator()
    {
        super();
        //
        singletonbyclass = new HashMap<>();
        objects = new LinkedList<>();
    }
    //
    void prepareSingleton(String classname)
    {
        try
        {
            var clazz = Class.forName(classname);
            Object singleton;
            //
            try
            {
                var constructor = clazz.getConstructor();
                singleton = constructor.newInstance();
            }
            catch (NoSuchMethodException e)
            {
                var constructor = clazz.getConstructor(Mediator.class);
                singleton = constructor.newInstance(this);
            }
            //
            singletonbyclass.put(clazz, singleton);
            objects.addLast(singleton);
        }
        catch (Exception e)
        {
            throw new IllegalArgumentException(e);
        }
    }
    //
    @SuppressWarnings("SameParameterValue")
    <T> void putSingleton(Class<T> clazz, T singleton)
    {
        singletonbyclass.put(clazz, singleton);
        objects.addLast(singleton);
    }
    //
    @SuppressWarnings("unchecked")
    public <T> T getSingleton(Class<T> clazz)
    {
        var singleton = singletonbyclass.get(clazz);
        //
        if (singleton == null) throw new IllegalStateException(String.format("singleton for %s not prepared or put", clazz));
        //
        return (T) singleton;
    }
    //
    @SuppressWarnings("unchecked")
    public <T> T createObject(Class<T> clazz, Object... restarguments)
    {
        try
        {
            var parameterclasses = new Class[restarguments.length + 1];
            var arguments = new Object[restarguments.length + 1];
            parameterclasses[0] = Mediator.class;
            arguments[0] = this;
            int i = 0;
            //
            while (i < restarguments.length)
            {
                var argument = restarguments[i++];
                var parameterclass = (argument == null) ? null : argument.getClass();
                parameterclasses[i] = parameterclass;
                arguments[i] = argument;
            }
            //
            var constructors = (Constructor<T>[]) clazz.getConstructors();
            var constructor = match(constructors, parameterclasses);
            T object = constructor.newInstance(arguments);
            objects.addLast(object);
            //
            return object;
        }
        catch (IllegalAccessException | InstantiationException | InvocationTargetException e)
        {
            throw new IllegalStateException(String.format("cannot create object for %s", clazz), e);
        }
    }
    //
    public <T> T createAndPostConstructObject(Class<T> clazz, Object... restarguments)
    {
        T object = createObject(clazz, restarguments);
        postConstruct();
        //
        return object;
    }
    //
    public void postConstruct()
    {
        while (!objects.isEmpty())
        {
            var object = objects.removeFirst();
            var clazz = object.getClass();
            //
            try
            {
                var method = clazz.getMethod("postConstruct");
                method.invoke(object);
            }
            catch (Exception e)
            {
                // do nothing
            }
        }
    }
    //
    private static <T> Constructor<T> match(Constructor<T>[] constructors, Class[] patternclasses)
    {
        for (var constructor : constructors)
        {
            if (match(constructor.getParameterTypes(), patternclasses)) return constructor;
        }
        //
        return null;
    }
    //
    @SuppressWarnings("unchecked")
    private static boolean match(Class[] parameterclasses, Class[] patternclasses)
    {
        if (parameterclasses.length != patternclasses.length) return false;
        //
        for (int i = 0; i < parameterclasses.length; i++)
        {
            var patternclass = patternclasses[i];
            //
            if (patternclass == null) continue;
            //
            var parameterclass = parameterclasses[i];
            //
            if (!parameterclass.isAssignableFrom(patternclass)) return false;
        }
        //
        return true;
    }
}