package calc.controller.command;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.Mediator;
import calc.io.Session;
import calc.model.ServiceModel;
import calc.model.Sheet;
import calc.view.ExceptionDialogFactory;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javax.swing.SwingUtilities.invokeLater;
/*
 * Created by andreasm on 24.09.18
 */
public final class PushModule implements Runnable
{
    private static final Logger logger = Logger.getAnonymousLogger();
    //
    private final Mediator mediator;
    //
    public PushModule(Mediator mediator)
    {
        super();
        //
        this.mediator = mediator;
    }
    //
    @Override
    public void run()
    {
        try
        {
            var sheet = mediator.getSingleton(Sheet.class);
            var servicemodel = mediator.getSingleton(ServiceModel.class);
            var name = sheet.getModuleName();
            var commentde = sheet.getModuleCommentDe();
            var exports = sheet.getModuleExports();
            var body = sheet.getProgram();
            var dependencies = sheet.getModules();
            var server = servicemodel.getServer();
            var emailaddress = servicemodel.getEmailAddress();
            var password = servicemodel.getPassword();
            var session = new Session(server);
            session.login(emailaddress, password);
            //
            try
            {
                session.pushModule(name, commentde, exports, body, dependencies);
            }
            finally
            {
                session.logout();
            }
        }
        catch (final Exception e)
        {
            logger.log(Level.WARNING, "cannot push module", e);
            //
            invokeLater(() ->
            {
                var dialogfactory = mediator.getSingleton(ExceptionDialogFactory.class);
                dialogfactory.showDialog("SAVE_MODULE", "CANNOT_SAVE_MODULE", e);
            });
        }
    }
}
