package calc.controller.command;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.model.Cell;
/*
 * Created by andreasm 04.12.13 10:00
 */
public final class CellFormula
{
    private final Cell cell;
    private final String formula;
    private final long lastupdate;
    //
    public CellFormula(Cell cell, String formula, long lastupdate)
    {
        super();
        //
        this.cell = cell;
        this.formula = formula;
        this.lastupdate = lastupdate;
    }
    //
    public Cell getCell()
    {
        return cell;
    }
    //
    public String getFormula()
    {
        return formula;
    }
    //
    public long getLastupdate()
    {
        return lastupdate;
    }
}