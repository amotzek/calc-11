package calc.controller.transfer;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import javax.swing.JComponent;
import calc.Mediator;
import calc.model.Selection;
import calc.model.Sheet;
import calc.model.Timeline;
import calc.view.SheetPanel;
/*
 * Created by andreasm 01.12.13 12:25
 */
public final class SheetPanelTransferHandler extends CellBlockTransferHandler
{
    private static final String EMPTY_STRING = "";
    //
    private final Mediator mediator;
    //
    public SheetPanelTransferHandler(Mediator mediator)
    {
        super();
        //
        this.mediator = mediator;
    }
    //
    @Override
    protected Transferable createTransferable(JComponent component)
    {
        var sheet = mediator.getSingleton(Sheet.class);
        var selection = mediator.getSingleton(Selection.class);
        var range = selection.getSelectedCellRange();
        //
        if (range == null) return null;
        //
        int rowindex = range.getRowIndex();
        int columnindex = range.getColumnIndex();
        int rowcount = range.getRowCount();
        int columncount = range.getColumnCount();
        var formulas = new String[columncount][rowcount];
        var values = new StringBuilder();
        //
        for (int j = 0; j < rowcount; j++)
        {
            if (j > 0) values.append('\n');
            //
            for (int i = 0; i < columncount; i++)
            {
                var cell = sheet.findCell(columnindex + i, rowindex + j);
                var formula = cell.getFormula();
                var value = cell.getValue();
                formulas[i][j] = formula;
                //
                if (i > 0) values.append('\t');
                //
                values.append((cell.hasValue() && value != null) ? value.toString() : EMPTY_STRING);
            }
        }
        //
        var block = new CellBlock(rowindex, columnindex, rowcount, columncount, formulas);
        //
        return new CellBlockTransferable(block, values.toString());
    }
    //
    @Override
    public boolean canImport(JComponent component, DataFlavor[] flavors)
    {
        if (component instanceof SheetPanel)
        {
            for (var flavor : flavors)
            {
                if (isCellBlockFlavor(flavor) || isStringFlavor(flavor)) return true;
            }
        }
        //
        return false;
    }
    //
    @Override
    public boolean importData(JComponent component, Transferable transferable)
    {
        var flavors = transferable.getTransferDataFlavors();
        //
        if (!canImport(component, flavors)) return false;
        //
        var sheet = mediator.getSingleton(Sheet.class);
        var selection = mediator.getSingleton(Selection.class);
        var timeline = mediator.getSingleton(Timeline.class);
        var range = selection.getSelectedCellRange();
        //
        if (range == null) return false;
        //
        int torowindex = range.getRowIndex();
        int tocolumnindex = range.getColumnIndex();
        //
        try
        {
            try
            {
                var block = (CellBlock) transferable.getTransferData(CellBlock.flavor);
                importBlock(block, torowindex, tocolumnindex, sheet, selection, timeline);
                //
                return true;
            }
            catch (UnsupportedFlavorException e)
            {
                // try StringFlavor
            }
            //
            var string = (String) transferable.getTransferData(DataFlavor.stringFlavor);
            var parser = new ExcelParser(string);
            var block = parser.getCellBlock(torowindex, tocolumnindex);
            importBlock(block, torowindex, tocolumnindex, sheet, selection, timeline);
        }
        catch (Exception e)
        {
            // return false
        }
        //
        return false;
    }
    //
    @Override
    protected void exportDone(JComponent component, Transferable transferable, int action)
    {
        if (action == MOVE)
        {
            try
            {
                var cellblock = (CellBlock) transferable.getTransferData(CellBlock.flavor);
                var sheet = mediator.getSingleton(Sheet.class);
                var selection = mediator.getSingleton(Selection.class);
                var timeline = mediator.getSingleton(Timeline.class);
                clearBlock(cellblock, sheet, selection, timeline);
            }
            catch (UnsupportedFlavorException | IOException e)
            {
                // do nothing
            }
        }
    }
}