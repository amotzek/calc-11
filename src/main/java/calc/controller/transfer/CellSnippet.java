package calc.controller.transfer;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.datatransfer.DataFlavor;
import java.io.Serializable;
/*
* Created by  andreasm 23.01.13 08:36
*/
final class CellSnippet implements Serializable
{
    static final DataFlavor flavor = new DataFlavor(CellSnippet.class, "Snippet");
    //
    private final int rowindex;
    private final int columnindex;
    private final String snippet;
    //
    CellSnippet(int rowindex, int columnindex, String snippet)
    {
        super();
        //
        this.rowindex = rowindex;
        this.columnindex = columnindex;
        this.snippet = snippet;
    }
    //
    int getRowIndex()
    {
        return rowindex;
    }
    //
    int getColumnIndex()
    {
        return columnindex;
    }
    //
    String getSnippet()
    {
        return snippet;
    }
}