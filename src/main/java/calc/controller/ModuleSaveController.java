package calc.controller;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.model.SaveDestination;
import calc.model.WizardModel;
import calc.view.ModuleSaveDialog;
/**
 * Created by andreasm on 16.09.18
 */
public final class ModuleSaveController extends AbstractWizardController<ModuleSaveDialog>
{
    public ModuleSaveController(WizardModel model, ModuleSaveDialog dialog)
    {
        super(model, dialog);
    }
    //
    @Override
    String getNextStep()
    {
        var step = getCurrentStep();
        var dialog = getDialog();
        //
        switch (step)
        {
            case WizardModel.START_STEP:
            {
                var name = dialog.getName();
                var commentde = dialog.getCommentDe();
                var exports = dialog.getExports();
                var destination = dialog.getSaveDestination();
                dialog.markNameError(name.isEmpty());
                dialog.markCommentDeError(commentde.isEmpty());
                dialog.markExportsError(exports.isEmpty());
                //
                if (name.isEmpty() || commentde.isEmpty() || exports.isEmpty()) return WizardModel.START_STEP;
                //
                if (destination == SaveDestination.CLOUD) return ModuleSaveDialog.LOGIN_STEP;
                //
                dialog.setModuleURI(String.format("local:%s", name));
                //
                return ModuleSaveDialog.DONE_STEP;
            }
            //
            case ModuleSaveDialog.LOGIN_STEP:
            {
                var name = dialog.getName();
                var server = dialog.getServer();
                var emailaddress = dialog.getEmailAddress();
                var password = new String(dialog.getPassword());
                dialog.markServerError(!server.startsWith("https://"));
                dialog.markEmailAddressError(emailaddress.isEmpty());
                dialog.markPasswordError(password.isEmpty());
                //
                if (!server.startsWith("https://") || emailaddress.isEmpty() || password.isEmpty()) return ModuleSaveDialog.LOGIN_STEP;
                //
                dialog.setModuleURI(String.format("%s/module/pull.binary?name=%s", server, name));
                //
                return ModuleSaveDialog.DONE_STEP;
            }
        }
        //
        assert false;
        //
        return step;
    }
    //
    @Override
    String getBackStep()
    {
        String step = getCurrentStep();
        //
        switch (step)
        {
            case ModuleSaveDialog.LOGIN_STEP:
                return WizardModel.START_STEP;
        }
        //
        return null;
    }
    //
    @Override
    boolean hasNextStep()
    {
        switch (getCurrentStep())
        {
            case WizardModel.START_STEP:
            case ModuleSaveDialog.LOGIN_STEP:
                return true;
        }
        //
        return false;
    }
    //
    @Override
    boolean hasBackStep()
    {
        return ModuleSaveDialog.LOGIN_STEP.equals(getCurrentStep());
    }
}
