package calc.controller.action;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.ResourceBundle;
import calc.Mediator;
import calc.model.Cell;
import calc.model.CellLockListener;
import calc.model.Sheet;
import static calc.view.Style.*;
import static javax.swing.SwingUtilities.invokeLater;
/*
 * Created by andreasm 16.02.13 15:20
 */
public final class ChangeSheetLock extends ConcurrentAction implements CellLockListener
{
    private Boolean locked;
    //
    public ChangeSheetLock(Mediator mediator)
    {
        super(mediator);
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        var sheet = mediator.getSingleton(Sheet.class);
        sheet.addCellLockListener(this);
        setNameAndIcon();
    }
    //
    public void cellLockChanged(Cell cell)
    {
        invokeLater(ChangeSheetLock.this::setNameAndIcon);
    }
    //
    public void run()
    {
        if (locked == null) return;
        //
        var sheet = mediator.getSingleton(Sheet.class);
        var cells = sheet.getCells();
        boolean notlocked = !locked;
        //
        for (var cell : cells)
        {
            cell.setLocked(notlocked);
        }
    }
    //
    private void setNameAndIcon()
    {
        boolean haslocks = checkLocks();
        //
        if (locked != null && haslocks == locked) return;
        //
        locked = haslocks;
        var bundle = mediator.getSingleton(ResourceBundle.class);
        //
        if (locked)
        {
            putValue(NAME, bundle.getString("UNLOCK_SHEET") + "   ");
            putValue(SMALL_ICON, getImageIcon("unlock_sheet.png"));
        }
        else
        {
            putValue(NAME, bundle.getString("LOCK_SHEET") + "   ");
            putValue(SMALL_ICON, null);
        }
    }
    //
    private boolean checkLocks()
    {
        var sheet = mediator.getSingleton(Sheet.class);
        var cells = sheet.getCells();
        //
        for (var cell : cells)
        {
            if(cell.isLocked()) return true;
        }
        //
        return false;
    }
}