package calc.controller.action;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.ResourceBundle;
import calc.Mediator;
import calc.model.Cell;
import calc.model.CellLockListener;
import calc.model.CellSelectionListener;
import calc.model.Selection;
import calc.model.Sheet;
import static calc.view.Style.*;
import static javax.swing.SwingUtilities.invokeLater;
/*
 * Created by andreasm 16.02.13 19:45
 */
public final class ChangeCellLock extends ConcurrentAction implements CellSelectionListener, CellLockListener
{
    public ChangeCellLock(Mediator mediator)
    {
        super(mediator);
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        var sheet = mediator.getSingleton(Sheet.class);
        var selection = mediator.getSingleton(Selection.class);
        sheet.addCellLockListener(this);
        selection.addCellSelectionListener(this);
        setNameAndIcon();
    }
    //
    public void cellLockChanged(Cell cell)
    {
        var selection = mediator.getSingleton(Selection.class);
        var selectedcell = selection.getSelectedCell();
        //
        if (cell != selectedcell) return;
        //
        invokeLater(ChangeCellLock.this::setNameAndIcon);
    }
    //
    public void cellSelected(Cell cell, boolean toporleft)
    {
        invokeLater(ChangeCellLock.this::setNameAndIcon);
    }
    //
    public void cellUnselected(Cell cell)
    {
        invokeLater(ChangeCellLock.this::setNameAndIcon);
    }
    //
    public void cellRangeChanged(Cell cell1, Cell cell2)
    {
    }
    //
    public void run()
    {
        var selection = mediator.getSingleton(Selection.class);
        var cell = selection.getSelectedCell();
        boolean locked = cell.isLocked();
        cell.setLocked(!locked);
    }
    //
    private void setNameAndIcon()
    {
        var selection = mediator.getSingleton(Selection.class);
        var bundle = mediator.getSingleton(ResourceBundle.class);
        var cell = selection.getSelectedCell();
        //
        if (cell == null)
        {
            putValue(NAME, bundle.getString("LOCK_CELL") + "   ");
            putValue(SMALL_ICON, null);
            setEnabled(false);
        }
        else if (cell.isLocked())
        {
            putValue(NAME, bundle.getString("UNLOCK_CELL") + "   ");
            putValue(SMALL_ICON, getImageIcon("unlock_cell.png"));
            setEnabled(true);
        }
        else
        {
            putValue(NAME, bundle.getString("LOCK_CELL") + "   ");
            putValue(SMALL_ICON, null);
            setEnabled(true);
        }
    }
}