package calc.controller.action;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.ResourceBundle;
import javax.swing.JOptionPane;
import calc.Mediator;
import calc.model.Sheet;
import calc.view.AppFrame;
/*
 * Created by andreasm 13.02.13 21:50
 */
abstract class SheetAction extends ConcurrentAction
{
    SheetAction(Mediator mediator)
    {
        super(mediator);
    }
    //
    final boolean keepUnsavedChanges(String question)
    {
        var sheet = mediator.getSingleton(Sheet.class);
        //
        if (!sheet.wasChanged()) return false;
        //
        var appframe = mediator.getSingleton(AppFrame.class);
        var bundle = mediator.getSingleton(ResourceBundle.class);
        var message = new StringBuilder();
        message.append(question);
        message.append(" ");
        message.append(bundle.getString("LOSE_UNSAVED_CHANGES"));
        var options = new String[] { "   " + bundle.getString("YES") + "   ", "   " + bundle.getString("NO") + "   " };
        //
        int option = JOptionPane.showOptionDialog(appframe, message.toString(), bundle.getString("UNSAVED_CHANGES"), JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[1]);
        //
        return option == 1;
    }
}