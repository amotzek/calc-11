package calc.controller.action;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ResourceBundle;
import calc.Mediator;
import calc.controller.CellValueFrameController;
import calc.model.Cell;
import calc.model.CellSelectionListener;
import calc.model.Selection;
import calc.view.AppFrame;
import calc.view.CellValueFrame;
import lisp.geometry.Picture;
import static calc.view.Style.*;
import static javax.swing.SwingUtilities.invokeLater;
/*
 * Created by andreasm 13.02.13 22:39
 */
public final class MagnifyCell extends AbstractMediatorAction implements CellSelectionListener
{
    private static final double AREA_FRACTION = 0.33d;
    private static final double PICTURE_BORDER = 20d;
    private static final int MIN_WIDTH = 80;
    private static final int MIN_HEIGHT = 80;
    private static final int WINDOW_TITLE = 20;
    //
    public MagnifyCell(Mediator mediator)
    {
        super(mediator);
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        var selection = mediator.getSingleton(Selection.class);
        var bundle = mediator.getSingleton(ResourceBundle.class);
        putValue(NAME, bundle.getString("MAGNIFY") + "   ");
        putValue(ACCELERATOR_KEY, createAccelerator(KeyEvent.VK_M));
        putValue(SMALL_ICON, getImageIcon("magnify.png"));
        setEnabled(false);
        selection.addCellSelectionListener(this);
    }
    //
    public void cellSelected(Cell cell, boolean toporleft)
    {
        invokeLater(MagnifyCell.this::setEnabled);
    }
    //
    public void cellUnselected(Cell cell)
    {
        invokeLater(MagnifyCell.this::setEnabled);
    }
    //
    public void cellRangeChanged(Cell cell1, Cell cell2)
    {
    }
    //
    public void actionPerformed(ActionEvent event)
    {
        var selection = mediator.getSingleton(Selection.class);
        var appframe = mediator.getSingleton(AppFrame.class);
        var cell = selection.getSelectedCell();
        var cellvalueframe = mediator.createAndPostConstructObject(CellValueFrame.class, cell);
        var controller = new CellValueFrameController(cellvalueframe);
        controller.connect();
        cellvalueframe.pack();
        var value = cell.getValue();
        //
        if (value instanceof Picture)
        {
            var picture = (Picture) value;
            var size = getSize(picture);
            //
            if (size != null) cellvalueframe.setSize(size);
        }
        //
        cellvalueframe.setLocationRelativeTo(appframe);
        cellvalueframe.setVisible(true);
    }
    //
    private Dimension getSize(Picture picture)
    {
        double sourcewidth = picture.getWidth() + PICTURE_BORDER;
        double sourceheight = picture.getHeight() + PICTURE_BORDER;
        //
        if (sourcewidth <= 0d || sourceheight <= 0d) return null;
        //
        var appframe = mediator.getSingleton(AppFrame.class);
        var framesize = appframe.getSize();
        double framewidth = framesize.getWidth();
        double frameheight = framesize.getHeight();
        double area = framewidth * frameheight * AREA_FRACTION;
        double areascale = area / (sourcewidth * sourceheight);
        double sidescale = Math.sqrt(areascale);
        int destinationwidth = (int) (sourcewidth * sidescale);
        int destinationheight = (int) (sourceheight * sidescale);
        int maxwidth = (int) framewidth - 50;
        int maxheight = (int) frameheight - 50;
        //
        if (destinationwidth > maxwidth) destinationwidth = maxwidth;
        if (destinationwidth < MIN_WIDTH) destinationwidth = MIN_WIDTH;
        if (destinationheight > maxheight) destinationheight = maxheight;
        if (destinationheight < MIN_HEIGHT) destinationheight = MIN_HEIGHT;
        //
        return new Dimension(destinationwidth, destinationheight + WINDOW_TITLE);
    }
    //
    private void setEnabled()
    {
        var selection = mediator.getSingleton(Selection.class);
        var cell = selection.getSelectedCell();
        setEnabled(cell != null);
    }
}