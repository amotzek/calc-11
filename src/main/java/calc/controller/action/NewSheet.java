package calc.controller.action;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.event.KeyEvent;
import java.util.ResourceBundle;
import calc.Mediator;
import calc.model.Cell;
import calc.model.FileReference;
import calc.model.Log;
import calc.model.Selection;
import calc.model.Sheet;
import calc.model.Timeline;
import static calc.view.Style.*;
/*
 * Created by andreasm 13.02.13 21:38
 */
public final class NewSheet extends SheetAction
{
    public NewSheet(Mediator mediator)
    {
        super(mediator);
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        var bundle = mediator.getSingleton(ResourceBundle.class);
        putValue(NAME, bundle.getString("NEW") + "...   ");
        putValue(ACCELERATOR_KEY, createAccelerator(KeyEvent.VK_N));
    }
    //
    @Override
    protected Runnable getRunnable()
    {
        var bundle = mediator.getSingleton(ResourceBundle.class);
        //
        if (keepUnsavedChanges(bundle.getString("WANT_NEW_SHEET"))) return null;
        //
        var timeline = mediator.getSingleton(Timeline.class);
        timeline.clear();
        //
        return this;
    }
    //
    public void run()
    {
        var sheet = mediator.getSingleton(Sheet.class);
        var selection = mediator.getSingleton(Selection.class);
        var filereference = mediator.getSingleton(FileReference.class);
        var log = mediator.getSingleton(Log.class);
        filereference.clear();
        selection.clear();
        swingBarrier();
        sheet.clear();
        log.clear();
        Cell cell = sheet.findCell("A", 1);
        selection.selectCell(cell);
    }
}