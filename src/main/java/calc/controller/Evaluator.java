package calc.controller;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.HashSet;
import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import calc.Mediator;
import calc.controller.combinator.CellValue;
import calc.controller.combinator.CellValues;
import calc.model.*;
import lisp.CannotEvalException;
import lisp.Closure;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.combinator.EnvironmentFactory;
import lisp.environment.Environment;
import lisp.environment.io.EnvironmentReader;
import lisp.geometry.combinator.GeometryModule;
/*
 * Created by andreasm on 16.05.12 at 20:09
 */
public final class Evaluator extends MediatorController implements CellValueListener, ProgramListener, Runnable
{
    private static final Symbol UNRESOLVED_DEPENDENCY = Symbol.createSymbol("unresolved-dependency");
    private static final CannotEvalException CYCLIC_DEPENDENCY = new CannotEvalException("cyclic dependency");
    private static final Symbol CELL_VALUE = Symbol.createSymbol("cell-value");
    private static final Symbol CELL_VALUES = Symbol.createSymbol("cell-values");
    private static final Logger logger = Logger.getAnonymousLogger();
    //
    private final LinkedBlockingQueue<Cell> agenda;
    private Environment parent;
    private volatile boolean programchanged;
    private volatile Closure closure;
    //
    public Evaluator(Mediator mediator)
    {
        super(mediator);
        //
        agenda = new LinkedBlockingQueue<>();
        programchanged = true;
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        var sheet = mediator.getSingleton(Sheet.class);
        sheet.addCellValueListener(this);
        sheet.addProgramListener(this);
    }
    //
    public void start()
    {
        /*
         * start cannot be called from constructor
         * because Evaluator has a reference to this
         */
        var thread = new Thread(this);
        thread.setDaemon(true);
        thread.start();
    }
    //
    public void interrupt()
    {
        do
        {
            agenda.clear();
            //
            try
            {
                closure.stop();
            }
            catch (NullPointerException e)
            {
                // do nothing
            }
        }
        while (!agenda.isEmpty());
    }
    //
    @Override
    public void cellValueChanged(Cell cell)
    {
        agenda.offer(cell);
    }
    //
    @Override
    public void programChanged()
    {
        programchanged = true;
    }
    //
    @Override
    public void run()
    {
        var calculation = mediator.getSingleton(Calculation.class);
        var log = mediator.getSingleton(Log.class);
        //
        while (true)
        {
            try
            {
                if (agenda.isEmpty()) calculation.stopped();
                //
                var cell = agenda.take();
                //
                if (isClean(cell)) continue;
                //
                int version = cell.getVersion();
                //
                if (isEmpty(cell))
                {
                    unsetCellValue(cell, version);
                    enQueueDependents(cell);
                    //
                    continue;
                }
                //
                if (isPartOfCycle(cell))
                {
                    setCellException(cell, CYCLIC_DEPENDENCY, version);
                    //
                    continue;
                }
                //
                calculation.started();
                //
                if (programchanged)
                {
                    programchanged = false;
                    //
                    try
                    {
                        parent = createEnvironment();
                        log.resetProgramException();
                    }
                    catch (CannotEvalException e)
                    {
                        log.setProgramException(e);
                    }
                }
                //
                try
                {
                    var value = evalCell(cell);
                    setCellValue(cell, value, version);
                    enQueueDependents(cell);
                }
                catch (CannotEvalException e)
                {
                    if (hasUnresolvedDependencies(e))
                    {
                        pushDirtyPrerequisites(cell);
                    }
                    else
                    {
                        setCellException(cell, e, version);
                        enQueueDependents(cell);
                    }
                }
                catch (RuntimeException e)
                {
                    unsetCellValue(cell, version);
                    enQueueDependents(cell);
                    logger.log(Level.WARNING, "runtime exception in evaluator", e);
                }
            }
            catch (InterruptedException e)
            {
                return;
            }
            catch (Throwable e)
            {
                logger.log(Level.WARNING, "throwable in evaluator", e);
            }
        }
    }
    //
    private static boolean isClean(Cell cell)
    {
        return !cell.isDirty();
    }
    //
    private static boolean isEmpty(Cell cell)
    {
        return !cell.hasFormula();
    }
    //
    private static boolean isPartOfCycle(Cell cell)
    {
        var visited = new HashSet<Cell>();
        var unvisited = new LinkedList<Cell>();
        unvisited.addFirst(cell);
        //
        while (!unvisited.isEmpty())
        {
            var current = unvisited.removeFirst();
            //
            if (visited.contains(current)) continue;
            //
            visited.add(current);
            //
            for (var next : current.dependsOn())
            {
                if (next == cell) return true;
                //
                unvisited.addFirst(next);
            }
        }
        //
        return false;
    }
    //
    private static boolean hasUnresolvedDependencies(CannotEvalException e)
    {
        return UNRESOLVED_DEPENDENCY == e.getSymbol();
    }
    //
    private Sexpression evalCell(Cell cell) throws CannotEvalException
    {
        closure = createClosure(cell);
        //
        try
        {
            return closure.eval();
        }
        finally
        {
            closure = null;
        }
    }
    //
    private void setCellValue(Cell cell, Sexpression value, int version)
    {
        if (cell.setValue(value, version))
        {
            var log = mediator.getSingleton(Log.class);
            log.resetCellException(cell);
        }
    }
    //
    private void unsetCellValue(Cell cell, int version)
    {
        if (cell.unsetValue(version))
        {
            var log = mediator.getSingleton(Log.class);
            log.resetCellException(cell);
        }
    }
    //
    private void setCellException(Cell cell, CannotEvalException e, int version)
    {
        if (cell.unsetValue(version))
        {
            var log = mediator.getSingleton(Log.class);
            log.setCellException(cell, e);
        }
    }
    //
    private void enQueueDependents(Cell cell)
    {
        for (var dependentcell : cell.prerequisiteFor())
        {
            dependentcell.dirty();
            agenda.offer(dependentcell);
        }
    }
    //
    private void pushDirtyPrerequisites(Cell cell)
    {
        for (var prerequisitecell : cell.dependsOn())
        {
            if (!prerequisitecell.isDirty()) continue;
            //
            agenda.offer(prerequisitecell);
        }
    }
    //
    private Environment createEnvironment() throws CannotEvalException
    {
        var sheet = mediator.getSingleton(Sheet.class);
        var modules = sheet.getModules();
        var program = sheet.getProgram();
        var environment = EnvironmentFactory.createEnvironment().copy();
        GeometryModule.augmentEnvironment(environment);
        var repository = new ModuleRepository();
        repository.addModules(modules);
        environment = repository.createEnvironment(environment);
        var reader = new EnvironmentReader(environment);
        reader.readFrom(program);
        //
        return environment;
    }
    //
    private Closure createClosure(Cell cell)
    {
        var sheet = mediator.getSingleton(Sheet.class);
        var child = new Environment(parent);
        child.add(false, CELL_VALUE, new CellValue(sheet, cell));
        child.add(false, CELL_VALUES, new CellValues(sheet, cell));
        var sexpression = cell.getSexpression();
        var closure = new Closure(child, sexpression);
        closure.setStoppable();
        //
        return closure;
    }
}