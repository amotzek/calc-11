package calc.controller;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.undo.AbstractUndoableEdit;
import calc.Mediator;
import calc.controller.command.CellFormula;
import calc.model.Cell;
import calc.model.Selection;
import calc.model.Timeline;
import calc.view.FormulaTextArea;
/*
 * Created by andreasm 02.12.13 07:54
 */
@SuppressWarnings("unused")
public final class FormulaTextAreaController extends SexpressionTextAreaController<FormulaTextArea> implements KeyListener, FocusListener
{
    private static final long DELAY = 500L;
    //
    private volatile Cell lastcell;
    //
    public FormulaTextAreaController(Mediator mediator)
    {
        super(mediator, FormulaTextArea.class);
    }
    //
    @Override
    public void postConstruct()
    {
        super.postConstruct();
        var textarea = mediator.getSingleton(FormulaTextArea.class);
        textarea.addKeyListener(this);
        textarea.addFocusListener(this);
    }
    //
    @Override
    public void keyTyped(KeyEvent event)
    {
    }
    //
    @Override
    public void keyPressed(KeyEvent event)
    {
    }
    //
    @Override
    public void keyReleased(KeyEvent event)
    {
        var selection = mediator.getSingleton(Selection.class);
        var timeline = mediator.getSingleton(Timeline.class);
        var textarea = mediator.getSingleton(FormulaTextArea.class);
        var cell = textarea.getCell();
        //
        if (cell == null) return;
        /*
         * must be processed in keyReleased
         * because otherwise the text in
         * the text area will not have changed
         */
        long now = event.getWhen();
        long lastupdate = cell.getLastUpdate();
        var oldformula = cell.getFormula();
        var newformula = textarea.getText();
        //
        if (cell.setFormula(newformula, now) && (cellChanged(cell) || oldEnough(now, lastupdate)))
        {
            var undokey = new UndoKey(selection, cell, lastupdate, now, oldformula, newformula);
            timeline.addEdit(undokey);
        }
    }
    //
    @Override
    public void focusGained(FocusEvent event)
    {
        var textarea = mediator.getSingleton(FormulaTextArea.class);
        //
        if (!textarea.isEditable())
        {
            var caret = textarea.getCaret();
            caret.setVisible(true);
        }
    }
    //
    @Override
    public void focusLost(FocusEvent e)
    {
    }
    //
    private boolean cellChanged(Cell cell)
    {
        if (lastcell == null)
        {
            lastcell = cell;
            //
            return false;
        }
        //
        if (cell == lastcell) return false;
        //
        lastcell = cell;
        //
        return true;
    }
    //
    private static boolean oldEnough(long now, long past)
    {
        return now - past > DELAY;
    }
    //
    private static class UndoKey extends AbstractUndoableEdit
    {
        private final Selection selection;
        private final CellFormula undocellformula;
        private final CellFormula redocellformula;
        //
        UndoKey(Selection selection, Cell cell, long lastupdate, long now, String oldformula, String newformula)
        {
            super();
            //
            this.selection = selection;
            //
            undocellformula = new CellFormula(cell, oldformula, lastupdate);
            redocellformula = new CellFormula(cell, newformula, now);
        }
        //
        @Override
        public void undo()
        {
            super.undo();
            //
            setCell(undocellformula);
        }
        //
        @Override
        public void redo()
        {
            super.redo();
            //
            setCell(redocellformula);
        }
        //
        private void setCell(CellFormula cellformula)
        {
            var selectedcell = selection.getSelectedCell();
            //
            if (selectedcell != null) selection.clear();
            //
            var cell = cellformula.getCell();
            String formula = cellformula.getFormula();
            long lastupdate = cellformula.getLastupdate();
            cell.setFormula(formula, lastupdate);
            //
            if (selectedcell != null) selection.selectCell(selectedcell);
        }
    }
}