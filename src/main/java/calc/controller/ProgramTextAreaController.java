package calc.controller;
/*
 * Copyright (C) 2015 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.Mediator;
import calc.view.ProgramTextArea;
/*
 * Created by Andreas on 22.09.2015.
 */
@SuppressWarnings("unused")
public final class ProgramTextAreaController extends SexpressionTextAreaController<ProgramTextArea>
{
    public ProgramTextAreaController(Mediator mediator)
    {
        super(mediator, ProgramTextArea.class);
    }
}
