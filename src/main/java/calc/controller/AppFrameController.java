package calc.controller;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.util.prefs.Preferences;
import calc.Mediator;
import calc.controller.command.ReadSheet;
import calc.controller.command.WriteSheet;
import calc.model.FileReference;
import calc.view.AppFrame;
import lisp.concurrent.RunnableQueueFactory;
/*
 * User: andreasm
 * Date: 17.04.13
 * Time: 18:25
 */
@SuppressWarnings("unused")
public final class AppFrameController extends MediatorController implements WindowListener
{
    public AppFrameController(Mediator mediator)
    {
        super(mediator);
    }
    //
    public void postConstruct()
    {
        var frame = mediator.getSingleton(AppFrame.class);
        frame.addWindowListener(this);
    }
    //
    @Override
    public void windowOpened(WindowEvent event)
    {
        var filereference = mediator.getSingleton(FileReference.class);
        var file = restoreFile(filereference);
        var readsheet = mediator.createAndPostConstructObject(ReadSheet.class, file);
        var runnablequeue = RunnableQueueFactory.getConcurrentRunnableQueue();
        runnablequeue.add(readsheet);
    }
    //
    @Override
    public void windowClosing(WindowEvent event)
    {
        var filereference = mediator.getSingleton(FileReference.class);
        var frame = mediator.getSingleton(AppFrame.class);
        frame.dispose();
        var file = storeFile(filereference);
        var writesheet = mediator.createAndPostConstructObject(WriteSheet.class, file);
        writesheet.run();
    }
    //
    @Override
    public void windowClosed(WindowEvent event)
    {
    }
    //
    @Override
    public void windowIconified(WindowEvent event)
    {
    }
    //
    @Override
    public void windowDeiconified(WindowEvent event)
    {
    }
    //
    @Override
    public void windowActivated(WindowEvent event)
    {
    }
    //
    @Override
    public void windowDeactivated(WindowEvent event)
    {
    }
    //
    private static File restoreFile(FileReference filereference)
    {
        var app = getPreferences();
        //
        if (app == null) return null;
        //
        var pathname = app.get("pathname", null);
        //
        if (pathname == null) return null;
        //
        var file = new File(pathname);
        filereference.setFile(file);
        //
        return file;
    }
    //
    private static File storeFile(FileReference filereference)
    {
        var file = filereference.getFile();
        var app = getPreferences();
        //
        if (app == null) return file;
        //
        if (file == null)
        {
            app.remove("pathname");
        }
        else
        {
            var pathname = file.getAbsolutePath();
            app.put("pathname", pathname);
        }
        //
        return file;
    }
    //
    private static Preferences getPreferences()
    {
        var root = Preferences.userRoot();
        //
        if (root == null) return null;
        //
        return root.node("sheetapp");
    }
}