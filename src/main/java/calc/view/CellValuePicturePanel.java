package calc.view;
/*
 * Copyright (C) 2012, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.Dimension;
import lisp.geometry.PicturePanel;
/*
 * Created by  andreasm 01.06.12 18:42
 */
final class CellValuePicturePanel extends PicturePanel
{
    CellValuePicturePanel()
    {
        super();
        //
        setPreferredSize(new Dimension(290, 165));
    }
}