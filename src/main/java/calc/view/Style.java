package calc.view;
/*
 * Copyright (C) 2012, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.*;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import javax.swing.*;
/*
 * Created by  andreasm 28.05.12 08:18
 */
public final class Style
{
    public static final double GOLDEN_RATIO = 1.618;
    public static final int GAP = 10;
    public static final int SMALL_GAP = 2;
    private static final int FONT_SIZE = 15;
    private static final int SMALL_FONT_SIZE = 10;
    private static final int SMALL_BUTTON_HEIGHT = 8 + SMALL_FONT_SIZE;
    public static final int MARGIN = 11 + FONT_SIZE;
    public static final int ROW_HEIGHT = 9 + FONT_SIZE;
    public static final int COLUMN_WIDTH = 9 * FONT_SIZE;
    public static final long REPAINT_DELAY = 100L;
    public static final int TOOLTIP_LIMIT = 80;
    public static final Color HEADER_SELECTION_COLOR = Color.LIGHT_GRAY;
    public static final Color RULER_SELECTION_COLOR = Color.LIGHT_GRAY;
    public static final Color CELL_SELECTION_COLOR = Color.GRAY;
    public static final Color MODULE_SELECTION_COLOR = new Color(255-16, 255-16, 255-16);
    public static final Color LOCK_COLOR = new Color(255, 255, 255-32);
    public static final Color ERROR_COLOR = new Color(255, 255-32, 255-32);
    //
    private Style()
    {
    }
    //
    static Font getPlainFont()
    {
        return getFont(Font.PLAIN, FONT_SIZE);
    }
    //
    static Font getBoldFont()
    {
        return getFont(Font.BOLD, FONT_SIZE);
    }
    //
    static Font getSmallFont()
    {
        return getFont(Font.PLAIN, SMALL_FONT_SIZE);
    }
    //
    static Font getMonospacedFont()
    {
        return new Font("Monospaced", Font.PLAIN, FONT_SIZE);
    }
    //
    private static Font getFont(int style, int size)
    {
        var interestingfamilies = new HashSet<String>();
        interestingfamilies.add("Segoe UI");
        interestingfamilies.add("Frutiger LT 55 Roman");
        interestingfamilies.add("Ubuntu");
        var environment = GraphicsEnvironment.getLocalGraphicsEnvironment();
        var families = environment.getAvailableFontFamilyNames();
        String foundfamily = null;
        //
        for (var family : families)
        {
            if (interestingfamilies.contains(family))
            {
                foundfamily = family;
                //
                break;
            }
        }
        //
        if (foundfamily == null) return new Font("SansSerif", style, size);
        //
        return new Font(foundfamily, style, size);
    }
    //
    public static ImageIcon getImageIcon(String iconfilename)
    {
        var url = Style.class.getResource("icon/" + iconfilename);
        //
        return new ImageIcon(url);
    }
    //
    static JScrollPane createScrollPane(JComponent component, boolean setminsize)
    {
        var preferredsize = component.getPreferredSize();
        var scrollpane = new JScrollPane(component);
        scrollpane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scrollpane.setAlignmentX(Component.LEFT_ALIGNMENT);
        //
        if (setminsize) scrollpane.setMinimumSize(preferredsize);
        //
        return scrollpane;
    }
    //
    static List<Image> getFrameIconImages()
    {
        var icon48 = getImageIcon("iconimage48.png");
        var icon16 = getImageIcon("iconimage16.png");
        var images = new LinkedList<Image>();
        images.add(icon48.getImage());
        images.add(icon16.getImage());
        //
        return images;
    }
    //
    public static KeyStroke createAccelerator(int keycode)
    {
        var toolkit = Toolkit.getDefaultToolkit();
        int keymask = toolkit.getMenuShortcutKeyMaskEx();
        //
        return KeyStroke.getKeyStroke(keycode, keymask);
    }
    //
    static Cursor getWaitCursor()
    {
        return Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
    }
    //
    static void adaptSize(JButton button1, JButton button2)
    {
        int width = max(getWidth(button1), getWidth(button2));
        adaptSize(button1, width);
        adaptSize(button2, width);
    }
    //
    static void adaptSize(JButton button)
    {
        int width = getWidth(button);
        adaptSize(button, width);
    }
    //
    private static void adaptSize(JButton button, int width)
    {
        var dimension = button.getPreferredSize();
        dimension.width = width;
        dimension.height = SMALL_BUTTON_HEIGHT;
        button.setPreferredSize(dimension);
    }
    //
    private static int getWidth(JButton button)
    {
        return button.getPreferredSize().width;
    }
    //
    private static int max(int... values)
    {
        int max = 0;
        //
        for (int value : values)
        {
            if (value > max) max = value;
        }
        //
        return max;
    }
    //
    public static void swingBarrier()
    {
        try
        {
            SwingUtilities.invokeAndWait(DO_NOTHING);
        }
        catch (Exception e)
        {
            // never occurs
        }
    }
    //
    private static final Runnable DO_NOTHING = () -> {};
}