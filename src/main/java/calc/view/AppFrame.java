package calc.view;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.CardLayout;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import javax.swing.JFrame;
import calc.Mediator;
import calc.model.*;
import static calc.view.Style.*;
import static javax.swing.SwingUtilities.invokeLater;
/*
 * Created by  andreasm 19.05.12 20:18
 */
public final class AppFrame extends JFrame implements FileListener, ViewModeListener
{
    private final Mediator mediator;
    private final CardLayout layout;
    //
    public AppFrame(Mediator mediator)
    {
        super();
        //
        this.mediator = mediator;
        //
        layout = new CardLayout();
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        var filereference = mediator.getSingleton(FileReference.class);
        var viewmode = mediator.getSingleton(ViewMode.class);
        var appmenubar = mediator.getSingleton(AppMenuBar.class);
        var sheetformulastatuspanel = mediator.getSingleton(SheetFormulaStatusPanel.class);
        var programpanel = mediator.getSingleton(ProgramPanel.class);
        var modulespanel = mediator.getSingleton(ModulesPanel.class);
        var bundle = mediator.getSingleton(ResourceBundle.class);
        setTitle(bundle.getString("SHEET"));
        //
        try
        {
            setIconImages(getFrameIconImages());
        }
        catch (Error e)
        {
            // no icon
        }
        //
        setLayout(layout);
        filereference.addFileListener(this);
        viewmode.addViewModeListener(this);
        setJMenuBar(appmenubar);
        add(sheetformulastatuspanel, ViewMode.SHEET);
        add(programpanel, ViewMode.PROGRAM);
        add(modulespanel, ViewMode.MODULES);
        showCard(ViewMode.SHEET);
    }
    //
    @Override
    public void fileChanged()
    {
        var filereference = mediator.getSingleton(FileReference.class);
        var bundle = mediator.getSingleton(ResourceBundle.class);
        var file = filereference.getFile();
        var title = (file == null) ? bundle.getString("SHEET") : MessageFormat.format(bundle.getString("SHEET_FILE"), file.getName());
        //
        invokeLater(() -> AppFrame.this.setTitle(title));
    }
    //
    @Override
    public void viewModeChanged()
    {
        var viewmode = mediator.getSingleton(ViewMode.class);
        var modename = viewmode.getModeName();
        //
        invokeLater(() -> AppFrame.this.showCard(modename));
    }
    //
    private void showCard(String modename)
    {
        var contentpane = getContentPane();
        layout.show(contentpane, modename);
    }
}