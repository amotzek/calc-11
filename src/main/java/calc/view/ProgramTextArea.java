package calc.view;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import javax.swing.JTextArea;
import static calc.view.Style.getMonospacedFont;
/*
 * Created by  andreasm 01.06.12 06:43
 */
public final class ProgramTextArea extends JTextArea
{
    public ProgramTextArea()
    {
        super(15, 60);
        //
        setAutoscrolls(true);
        setLineWrap(true);
        setFont(getMonospacedFont());
    }
}