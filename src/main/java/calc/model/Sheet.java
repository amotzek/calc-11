package calc.model;
/*
 * Copyright (C) 2012, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import lisp.CannotEvalException;
import lisp.List;
import lisp.Rational;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.module.Module;
/*
 * Created by andreasm on 15.05.12 at 21:06
 */
public final class Sheet
{
    private final Row[] rows;
    private final Column[] columns;
    private final HashMap<Coordinates, Cell> cellsbycoordinates;
    private final CellListenerDelegate celllisteners;
    private final LinkedList<ProgramListener> programlisteners;
    private final ModuleRepository repository;
    private String program;
    private String modulename;
    private String modulecommentde;
    private Collection<String> moduleexports;
    //
    public Sheet()
    {
        super();
        //
        rows = new Row[64];
        columns = new Column[32];
        cellsbycoordinates = new HashMap<>(100);
        celllisteners = new CellListenerDelegate();
        programlisteners = new LinkedList<>();
        repository = new ModuleRepository();
        moduleexports = Collections.emptyList();
        //
        createColumns();
        createRows();
    }
    //
    public void addCellValueListener(CellValueListener listener)
    {
        celllisteners.addCellValueListener(listener);
    }
    //
    public void removeCellValueListener(CellValueListener listener)
    {
        celllisteners.removeCellValueListener(listener);
    }
    //
    public void addCellLockListener(CellLockListener listener)
    {
        celllisteners.addCellLockListener(listener);
    }
    //
    public synchronized void addProgramListener(ProgramListener listener)
    {
        programlisteners.add(listener);
    }
    //
    private void programChanged()
    {
        for (var listener : programlisteners)
        {
            listener.programChanged();
        }
    }
    //
    public Row[] getRows()
    {
        return rows;
    }
    //
    public Column[] getColumns()
    {
        return columns;
    }
    //
    public synchronized Collection<Cell> getCells()
    {
        return new LinkedList<>(cellsbycoordinates.values());
    }
    //
    public synchronized void clear()
    {
        repository.clearModules();
        modulename = null;
        modulecommentde = null;
        moduleexports = Collections.emptyList();
        program = null;
        var cells = cellsbycoordinates.values();
        //
        for (var cell : cells)
        {
            cell.setFormula("", 0L);
            cell.setLocked(false);
            cell.setUnchanged();
        }
        //
        programChanged();
    }
    //
    public synchronized boolean wasChanged()
    {
        for (var cell : cellsbycoordinates.values())
        {
            if (cell.wasChanged()) return true;
        }
        //
        return false;
    }
    //
    public synchronized void setUnchanged()
    {
        for (var cell : cellsbycoordinates.values())
        {
            cell.setUnchanged();
        }
    }
    //
    public void reevaluate()
    {
        var cells = getCells();
        //
        for (var cell : cells)
        {
            cell.reevaluate();
        }
    }
    //
    public Cell findCell(String columnlabel, int rowindex)
    {
        try
        {
            int columnindex = Column.toIndex(columnlabel);
            //
            return findCell(columnindex, rowindex);
        }
        catch (IllegalArgumentException e)
        {
            // cell does not exist
        }
        //
        return null;
    }
    //
    public Cell findCell(int columnindex, int rowindex)
    {
        var column = columns[columnindex];
        var row = rows[rowindex];
        //
        return findCell(column, row);
    }
    //
    public Cell findCell(Sexpression sexpression) throws CannotEvalException
    {
        try
        {
            var list = (List) sexpression;
            var first = list.first();
            list = list.rest();
            var second = list.first();
            var column = findColumn(first);
            var row = findRow(second);
            //
            return findCell(column, row);
        }
        catch (NullPointerException e)
        {
            throw new CannotEvalException("coordinates for a cell must be a list with two non-nil elements");
        }
        catch (ClassCastException e)
        {
            throw new CannotEvalException("coordinates for a cell must be a list with a symbol and an integer");
        }
    }
    //
    public synchronized Cell findCell(Column column, Row row)
    {
        var coordinates = new Coordinates(row, column);
        //
        return cellsbycoordinates.computeIfAbsent(coordinates, coordinates1 -> new Cell(celllisteners, column, row));
    }
    //
    public Collection<Cell> findCells(Sexpression sexpression) throws CannotEvalException
    {
        try
        {
            var list = (List) sexpression;
            var first = list.first();
            list = list.rest();
            var second = list.first();
            list = list.rest();
            var third = list.first();
            list = list.rest();
            var fourth = list.first();
            var left = findColumn(first);
            var right = findColumn(third);
            var up = findRow(second);
            var down = findRow(fourth);
            var cells = new LinkedList<Cell>();
            //
            for (int rowindex = up.getIndex(); rowindex <= down.getIndex(); rowindex++)
            {
                var row = rows[rowindex];
                //
                for (int columnindex = left.getIndex(); columnindex <= right.getIndex(); columnindex++)
                {
                    var column = columns[columnindex];
                    var cell = findCell(column, row);
                    cells.addFirst(cell);
                }
            }
            //
            return cells;
        }
        catch (NullPointerException e)
        {
            throw new CannotEvalException("coordinates for a cell range must be a list with four non-nil elements");
        }
        catch (ClassCastException e)
        {
            throw new CannotEvalException("coordinates for a cell must be a list with a symbol and an integer");
        }
    }
    //
    private void createColumns()
    {
        for (int index = 1; index < columns.length; index++)
        {
            var label = Column.toLabel(index);
            columns[index] = new Column(index, label);
        }
    }
    //
    private void createRows()
    {
        for (int index = 1; index < rows.length; index++)
        {
            rows[index] = new Row(index);
        }
    }
    //
    private synchronized Row findRow(Sexpression sexpression) throws CannotEvalException
    {
        var rational = (Rational) sexpression;
        int index = rational.intValue();
        //
        if (index < 1 || index >= rows.length) throw new CannotEvalException("invalid row");
        //
        var row = rows[index];
        //
        if (row == null)
        {
            row = new Row(index);
            rows[index] = row;
        }
        //
        return row;
    }
    //
    private synchronized Column findColumn(Sexpression sexpression) throws CannotEvalException
    {
        var symbol = (Symbol) sexpression;
        var label = symbol.getName();
        //
        try
        {
            int index = Column.toIndex(label);
            //
            if (index < 1 || index >= columns.length) throw new CannotEvalException("invalid column");
            //
            var column = columns[index];
            //
            if (column == null)
            {
                column = new Column(index, label);
                columns[index] = column;
            }
            //
            return column;
        }
        catch (IllegalArgumentException e)
        {
            throw new CannotEvalException(e.getMessage());
        }
    }
    //
    public synchronized String getProgram()
    {
        return program;
    }
    //
    public synchronized void setProgram(String program)
    {
        this.program = program;
        programChanged();
        reevaluate();
    }
    //
    public synchronized Collection<Module> getModules()
    {
        return repository.getModules();
    }
    //
    public synchronized void setModules(Collection<Module> modules) throws IOException
    {
        repository.clearModules();
        repository.addModules(modules);
        repository.satisfyDependencies();
        programChanged();
        reevaluate();
    }
    //
    public synchronized String getModuleName()
    {
        return modulename;
    }
    //
    public synchronized void setModuleName(String modulename)
    {
        this.modulename = modulename;
    }
    //
    public synchronized String getModuleCommentDe()
    {
        return modulecommentde;
    }
    //
    public synchronized void setModuleCommentDe(String modulecommentde)
    {
        this.modulecommentde = modulecommentde;
    }
    //
    public synchronized Collection<String> getModuleExports()
    {
        return moduleexports;
    }
    //
    public synchronized void setModuleExports(Collection<String> moduleexports)
    {
        this.moduleexports = moduleexports;
    }
}