package calc.model;
/*
 * Copyright (C) 2014 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.HashMap;
import java.util.LinkedList;
import lisp.CannotEvalException;
import lisp.Chars;
/*
 * Created by andreasm on 14.10.2014.
 */
public final class Log
{
    private final HashMap<Cell, CannotEvalException> exceptionsbycell;
    private final LinkedList<LogListener> listeners;
    private CannotEvalException programexception;
    //
    public Log()
    {
        super();
        //
        exceptionsbycell = new HashMap<>(10);
        listeners = new LinkedList<>();
    }
    //
    public synchronized void addLogListener(LogListener listener)
    {
        listeners.add(listener);
    }
    //
    private void logChanged()
    {
        for (var listener : listeners)
        {
            listener.logChanged();
        }
    }
    //
    public synchronized LogEntry getLogEntry()
    {
        if (programexception != null) return new LogEntry(programexception, null, null);
        //
        var entryset = exceptionsbycell.entrySet();
        //
        if (entryset.isEmpty()) return null;
        //
        CannotEvalException selectedexception = null;
        Cell selectedcell = null;
        int length = 0;
        //
        for (var entry : entryset)
        {
            var exception = entry.getValue();
            var sexpression = exception.getSexpression();
            String string;
            //
            if (sexpression instanceof Chars)
            {
                var chars = (Chars) sexpression;
                string = chars.getString();
            }
            else if (sexpression != null)
            {
                string = sexpression.toString();
            }
            else
            {
                string = "nil";
            }
            //
            if (length < string.length())
            {
                selectedexception = exception;
                selectedcell = entry.getKey();
                length = string.length();
            }
        }
        //
        if (selectedcell == null) return null;
        //
        var row = selectedcell.getRow();
        var column = selectedcell.getColumn();
        //
        return new LogEntry(selectedexception, row, column);
    }
    //
    public synchronized void clear()
    {
        programexception = null;
        exceptionsbycell.clear();
        logChanged();
    }
    //
    public synchronized void setProgramException(CannotEvalException e)
    {
        programexception = e;
        logChanged();
    }
    //
    public synchronized void resetProgramException()
    {
        programexception = null;
        logChanged();
    }
    //
    public synchronized void setCellException(Cell cell, CannotEvalException e)
    {
        exceptionsbycell.put(cell, e);
        logChanged();
    }
    //
    public synchronized void resetCellException(Cell cell)
    {
        exceptionsbycell.remove(cell);
        logChanged();
    }
}