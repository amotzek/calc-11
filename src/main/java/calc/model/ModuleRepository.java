package calc.model;
/*
 * Copyright (C) 2015, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.io.ModuleReader;
import lisp.module.AbstractRepository;
import lisp.module.Module;
import propertyset.VisitorException;
import java.io.IOException;
import java.net.URI;
/*
 * Created by Andreas on 29.08.2015.
 */
public final class ModuleRepository extends AbstractRepository
{
    private final ModuleReader reader;
    //
    public ModuleRepository()
    {
        reader = new ModuleReader();
    }
    //
    @Override
    protected Module loadModule(URI uri) throws IOException
    {
        try
        {
            return reader.readFrom(uri);
        }
        catch (VisitorException e)
        {
            throw new IOException(e);
        }
    }
}