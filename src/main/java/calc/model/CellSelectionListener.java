package calc.model;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created by andreasm on 21.05.12 at 11:49
 */
public interface CellSelectionListener
{
    void cellSelected(Cell cell, boolean toporleft);
    //
    void cellUnselected(Cell cell);
    //
    void cellRangeChanged(Cell cell1, Cell cell2);
}