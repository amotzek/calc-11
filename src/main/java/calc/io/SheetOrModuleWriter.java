package calc.io;
/*
 * Copyright (C) 2015 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.model.Sheet;
import lisp.module.Module;
import lisp.module.ModuleDependency;
import propertyset.PropertySet;
/**
 * Created by andreasm on 10.06.15
 */
abstract class SheetOrModuleWriter extends SheetReaderOrWriter
{
    SheetOrModuleWriter(Sheet sheet)
    {
        super(sheet);
    }
    //
    static PropertySet toPropertySet(Module module)
    {
        var name = module.getName();
        int version = module.getVersion();
        var uri = module.getURI();
        var commentde = module.getCommentDe();
        var commenten = module.getCommentEn();
        var exports = module.getExports();
        var body = module.getBody();
        var propertyset = new PropertySet();
        propertyset.setName("module");
        propertyset.addChild("name", name);
        propertyset.addChild("version", Integer.toString(version));
        propertyset.addChild("uri", uri.toString());
        propertyset.addChild("comment-de", commentde);
        propertyset.addChild("comment-en", commenten);
        //
        for (var export : exports)
        {
            propertyset.addChild("export", export);
        }
        //
        propertyset.addChild("body", body);
        var dependencies = module.getDependencies();
        //
        for (var dependency : dependencies)
        {
            propertyset.addChild(toPropertySet(dependency));
        }
        //
        return propertyset;
    }
    //
    private static PropertySet toPropertySet(ModuleDependency dependency)
    {
        var name = dependency.getName();
        int version = dependency.getMinimumRequiredVersion();
        var uri = dependency.getURI();
        var propertyset = new PropertySet();
        propertyset.setName("dependency");
        propertyset.addChild("name", name);
        propertyset.addChild("minimum-required-version", Integer.toString(version));
        propertyset.addChild("uri", uri.toString());
        //
        return propertyset;
    }
}