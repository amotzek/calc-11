package calc.io;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import calc.model.Cell;
import calc.model.Sheet;
import propertyset.PropertySet;
import xml.propertyset.PropertySetFormatter;
/*
 * Created by  andreasm 22.05.12 19:37
 */
public final class SheetWriter extends SheetOrModuleWriter
{
    private String xml;
    //
    public SheetWriter(Sheet sheet)
    {
        super(sheet);
    }
    //
    public void run()
    {
        var propertyset = toPropertySet(sheet);
        var formatter = new PropertySetFormatter(propertyset);
        xml = formatter.getXML();
    }
    //
    @SuppressWarnings("unused")
    public String getXML()
    {
        return xml;
    }
    //
    public void writeTo(File file) throws IOException
    {
        boolean wasdefault = false;
        //
        if (file == null)
        {
            file = getDefaultFile();
            wasdefault = true;
        }
        //
        var stream = new FileOutputStream(file);
        //
        try (var writer = new OutputStreamWriter(stream, StandardCharsets.UTF_8))
        {
            writer.write(xml);
            writer.flush();
        }
        //
        if (wasdefault) return;
        //
        sheet.setUnchanged();
    }
    //
    private static PropertySet toPropertySet(Sheet sheet)
    {
        var propertyset = new PropertySet();
        propertyset.setName("sheet");
        var cells = sheet.getCells();
        //
        for (var cell : cells)
        {
            if (cell.hasFormula()) propertyset.addChild(toPropertySet(cell));
        }
        //
        var program = sheet.getProgram();
        //
        if (program != null) propertyset.addChild("program", program);
        //
        var modules = sheet.getModules();
        var modulename = sheet.getModuleName();
        var modulecommentde = sheet.getModuleCommentDe();
        var moduleexports = sheet.getModuleExports();
        //
        for (var module : modules)
        {
            propertyset.addChild(toPropertySet(module));
        }
        //
        if (modulename != null) propertyset.addChild("module-name", modulename);
        //
        if (modulecommentde != null) propertyset.addChild("module-description", modulecommentde); // TODO umbenennen
        //
        if (moduleexports != null) propertyset.addChild("module-exports", toString(moduleexports)); // TODO einzelne Elemente erzeugen
        //
        return propertyset;
    }
    //
    private static PropertySet toPropertySet(Cell cell)
    {
        var column = cell.getColumn();
        var row = cell.getRow();
        var formula = cell.getFormula();
        boolean locked = cell.isLocked();
        var propertyset = new PropertySet();
        propertyset.setName("cell");
        propertyset.addChild("column", column.getLabel());
        propertyset.addChild("row", row.getLabel());
        propertyset.addChild("formula", formula);
        //
        if (locked) propertyset.addChild("locked", null);
        //
        return propertyset;
    }
    //
    private static String toString(Collection<String> exports)
    {
        var builder = new StringBuilder();
        //
        for (var export : exports)
        {
            if (builder.length() > 0) builder.append(' ');
            //
            builder.append(export);
        }
        //
        return builder.toString();
    }
}