package calc.io;
/*
 * Copyright (C) 2015 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.model.Sheet;
import lisp.module.Module;
import xml.propertyset.PropertySetFormatter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.nio.charset.StandardCharsets;
/**
 * Created by andreasm on 10.06.15
 */
public final class ModuleWriter extends SheetOrModuleWriter
{
    private final Module module;
    private String xml;
    //
    public ModuleWriter(Sheet sheet, Module module)
    {
        super(sheet);
        //
        this.module = module;
    }
    //
    public void run()
    {
        var propertyset = toPropertySet(module);
        var formatter = new PropertySetFormatter(propertyset);
        xml = formatter.getXML();
    }
    //
    @SuppressWarnings("unused")
    public String getXML()
    {
        return xml;
    }
    //
    public void writeTo(URI uri) throws IOException
    {
        var url = uri.toURL();
        var protocol = url.getProtocol();
        //
        if (!"local".equals(protocol)) throw new IOException("cannot write to " + url);
        //
        var connection = url.openConnection();
        var stream = connection.getOutputStream();
        var writer = new OutputStreamWriter(stream, StandardCharsets.UTF_8);
        writer.write(xml);
        writer.flush();
        writer.close();
    }
}