package calc.io;
/*
 * Copyright (C) 2015, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.module.Module;
import propertyset.ObjectStructure;
import propertyset.VisitorException;
import xml.propertyset.PropertySetParser;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
/**
 * Created by andreasm on 07.06.15
 */
public final class ModuleReader
{
    private String cookie;
    //
    public ModuleReader()
    {
        super();
    }
    //
    public Module readFrom(URI uri) throws IOException, VisitorException
    {
        var url = uri.toURL();
        var connection = url.openConnection();
        restoreCookie(connection);
        var parser = createParser(connection);
        saveCookie(connection);
        //
        return parseModule(parser);
    }
    //
    private void restoreCookie(URLConnection connection)
    {
        if (cookie == null) return;
        //
        connection.setRequestProperty("Cookie", cookie);
    }
    //
    private PropertySetParser createParser(URLConnection connection) throws IOException
    {
        PropertySetParser parser;
        //
        try (var stream = connection.getInputStream())
        {
            var reader = new InputStreamReader(stream, StandardCharsets.UTF_8);
            parser = new PropertySetParser(reader);
        }
        //
        return parser;
    }
    //
    private void saveCookie(URLConnection connection)
    {
        var cookie = connection.getHeaderField("Set-Cookie");
        //
        if (cookie != null) this.cookie = cookie;
    }
    //
    private Module parseModule(PropertySetParser parser) throws VisitorException
    {
        var propertyset = parser.getPropertySet();
        var visitor = new ModuleVisitor();
        var structure = new ObjectStructure(propertyset);
        structure.traverseWith(visitor);
        //
        return visitor.getModule();
    }
}