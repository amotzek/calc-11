package calc.io;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import calc.model.Log;
import calc.model.Sheet;
import propertyset.ObjectStructure;
import propertyset.VisitorException;
import xml.propertyset.PropertySetParser;
/*
 * Created by  andreasm 22.05.12 20:41
 */
public final class SheetReader extends SheetReaderOrWriter
{
    private final Log log;
    private PropertySetParser parser;
    private long lastupdate;
    //
    public SheetReader(Sheet sheet, Log log)
    {
        super(sheet);
        //
        this.log = log;
    }
    //
    public void readFrom(File file) throws IOException
    {
        if (file == null) file = getDefaultFile();
        //
        lastupdate = file.lastModified();
        var stream = new FileInputStream(file);
        //
        try (var reader = new InputStreamReader(stream, StandardCharsets.UTF_8))
        {
            parser = new PropertySetParser(reader);
        }
    }
    //
    public void run() throws VisitorException, IOException
    {
        var propertyset = parser.getPropertySet();
        var cellvisitor = new CellVisitor(sheet, lastupdate);
        var programvisitor = new ProgramVisitor(sheet);
        var modulevisitor = new ModuleVisitor();
        var moduleexportvisitor = new ModuleExportVisitor(sheet);
        var structure = new ObjectStructure(propertyset);
        sheet.clear();
        log.clear();
        structure.traverseWith(moduleexportvisitor);
        structure.traverseWith(modulevisitor);
        sheet.setModules(modulevisitor.getModules());
        structure.traverseWith(programvisitor);
        structure.traverseWith(cellvisitor);
        sheet.setUnchanged();
    }
}