/*
 * Copyright (C) 2005, 2014 Andreas Motzek andreas-motzek@t-online.de
 * 
 * This file is part of the Functional Geometry package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created on 10.04.2005
 */
package lisp.geometry.combinator;
//
import lisp.CannotEvalException;
import lisp.Sexpression;
import lisp.environment.Environment;
import java.awt.geom.AffineTransform;
/*
 * @author Andreasm
 */
final class Above extends lisp.geometry.combinator.PictureCombinator
{
    Above()
    {
        super(0, 4);
    }
    //
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        double ratiobottom = getRational(arguments, 0).doubleValue();
        double ratiotop = getRational(arguments, 1).doubleValue();
        var picturebottom = getPicture(arguments, 2);
        var picturetop = getPicture(arguments, 3);
        //
        if (ratiobottom <= 0d || ratiotop <= 0d) throw new CannotEvalException("ratios must be positive in above");
        //
        double sourcewidthtop = picturetop.getWidth();
        double sourceheighttop = picturetop.getHeight();
        double sourcewidthbottom = picturebottom.getWidth();
        double sourceheightbottom = picturebottom.getHeight();
        //
        double width = Math.max(sourcewidthtop, sourcewidthbottom);
        double height = sourceheighttop + sourceheightbottom;
        double ratio = ratiotop + ratiobottom;
        double inverseratio = 1d / ratio;
        //
        double destinationheighttop = height * ratiotop * inverseratio;
        double destinationheightbottom = height * ratiobottom * inverseratio;
        //
        double scaletop = destinationheighttop / sourceheighttop;
        double scalebottom = destinationheightbottom / sourceheightbottom;
        double scaleleft = width / sourcewidthtop;
        double scaleright = width / sourcewidthbottom;
        //
        var transformtop = AffineTransform.getScaleInstance(scaleleft, scaletop);
        var transformbottom = new AffineTransform(scaleright, 0d, 0d, scalebottom, 0d, destinationheighttop);
        var transformedtop = new TransformedPicture(transformtop, picturetop, width, destinationheighttop);
        var transformedbottom = new TransformedPicture(transformbottom, picturebottom, width, destinationheightbottom);
        var overlaypicture = new OverlayPicture(transformedtop.simplify(), transformedbottom.simplify(), width, height);
        //
        return overlaypicture.simplify();
    }
    //
    @Override
    public String toString()
    {
        return "above";
    }
}