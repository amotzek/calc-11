package lisp.geometry.combinator;
/*
 * Copyright (C) 2005, 2014, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Functional Geometry package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created on 10.04.2005
 */
import lisp.geometry.Picture;
import lisp.Constant;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
/*
 * @author Andreasm
 */
final class OverlayPicture extends Constant implements Picture
{
    private final Collection<Picture> pictures;
    private final double width;
    private final double height;
    //
    OverlayPicture(Picture picture0, Picture picture1, double width, double height)
    {
        this(Arrays.asList(picture0, picture1), width, height);
    }
    //
    private OverlayPicture(Collection<Picture> pictures, double width, double height)
    {
        super();
        //
        this.pictures = pictures;
        this.width = width;
        this.height = height;
    }
    //
    @Override
    public String getType()
    {
        return "picture";
    }
    //
    @Override
    public double getWidth()
    {
        return width;
    }
    //
    @Override
    public double getHeight()
    {
        return height;
    }
    //
    @Override
    public void paint(Graphics2D graphics, AffineTransform transform)
    {
        for (var picture : pictures)
        {
            picture.paint(graphics, transform);
        }
    }
    //
    @Override
    public Picture simplify()
    {
        if (someOverlayPicture())
        {
            var unionpictures = new ArrayList<Picture>(pictures.size());
            //
            for (var picture : pictures)
            {
                if (picture instanceof OverlayPicture)
                {
                    var overlaypicture = (OverlayPicture) picture;
                    unionpictures.addAll(overlaypicture.pictures);
                }
                else
                {
                    unionpictures.add(picture);
                }
            }
            //
            return new OverlayPicture(unionpictures, width, height);
        }
        //
        return this;
    }
    //
    private boolean someOverlayPicture()
    {
        for (var picture : pictures)
        {
            if (picture instanceof OverlayPicture) return true;
        }
        //
        return false;
    }
    //
    Picture applyTransform(AffineTransform transform, double width, double height)
    {
        if (manyTransformedPictures())
        {
            var transformedpictures = new ArrayList<Picture>(pictures.size());
            //
            for (var picture : pictures)
            {
                var transformedpicture = new TransformedPicture(transform, picture, width, height);
                transformedpictures.add(transformedpicture.simplify());
            }
            //
            return new OverlayPicture(transformedpictures, width, height);
        }
        //
        return null;
    }
    //
    private boolean manyTransformedPictures()
    {
        int count = 0;
        //
        for (var picture : pictures)
        {
            if (!(picture instanceof TransformedPicture)) count++;
        }
        //
        return count < 2;
    }
}