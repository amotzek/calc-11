package lisp.geometry.combinator;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Functional Geometry package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Symbol;
import lisp.environment.Environment;
/*
 * Created by  andreasm 01.06.12 18:14
 */
public final class GeometryModule
{
    private GeometryModule()
    {
    }
    //
    public static void augmentEnvironment(Environment environment)
    {
        environment.add(false, Symbol.createSymbol("above"), new Above());
        environment.add(false, Symbol.createSymbol("beside"), new Beside());
        environment.add(false, Symbol.createSymbol("flip"), new Flip());
        environment.add(false, Symbol.createSymbol("grid"), new Grid());
        environment.add(false, Symbol.createSymbol("overlay"), new Overlay());
        environment.add(false, Symbol.createSymbol("rotate-left"), new RotateLeft());
        environment.add(false, Symbol.createSymbol("translate"), new Translate());
        environment.add(false, Symbol.createSymbol("scale"), new Scale());
    }
}