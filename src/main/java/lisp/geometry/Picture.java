package lisp.geometry;
/*
 * Copyright (C) 2005, 2014 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Functional Geometry package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created on 10.04.2005
 */
import lisp.Sexpression;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
/*
 * @author Andreasm
 */
public interface Picture extends Sexpression
{
    double getWidth();
    //
    double getHeight();
    //
    void paint(Graphics2D graphics, AffineTransform transform);
    //
    Picture simplify();
}